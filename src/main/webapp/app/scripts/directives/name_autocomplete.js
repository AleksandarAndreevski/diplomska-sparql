/**
 * Created by Aleksandar on 18/5/2015.
 */

FirstApp.directive(
  'attrNameAutocomplete',
  [
    function () {
      return {
        restrict: 'EA',
        // templateUrl: 'templates/textarea.html',
        scope: {
          /**
           * the query that will be generated with
           * autocomplete
           */
          semResourceObject: '=',
          /**
           * The result of the query execution. Null
           * if execute is not invoked.
           */
          result: '=',
          /*
           * List of endpoints that should be shown in
           * the combo
           */
          endpoints: '=',
          /*
           * The default selected endpoint, or the one
           * used in absence of the 'endpoints' list
           */
          defaultEndpoint: '@',
          /**
           * Whether to show or hide the execute
           * button
           */
          hideExecute: '='
        },
        link: function (scope, el, attrs) {
          var sem_links = [];
          angular.forEach(scope.semResourceObject, function (value, key) {
            sem_links.push(key);
          });
          var input = $(el).find("input");
          input.textcomplete([{
            match: /(\w*)$/,
            search: function (term, callback) {
              callback($.map(sem_links, function (element) {
                return element.indexOf(term) >= 0 ? element : null;
              }));
            },
            replace: function (value) {
              return value;
            },
            index: 1
          }]);
        },
        controller: function ($scope) {

        }
      };
    }
  ]);







