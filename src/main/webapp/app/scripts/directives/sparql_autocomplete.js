/**
 * Created by Aleksandar on 18/5/2015.
 */

FirstApp.directive(
  'sparqlComplete',
  [
    'Prefixes',
    'Sparql',
    function (Prefixes, Sparql) {
      return {
        restrict: 'EA',
        // templateUrl: 'templates/textarea.html',
        scope: {
          /**
           * the query that will be generated with
           * autocomplete
           */
          query: '=',
          /**
           * The result of the query execution. Null
           * if execute is not invoked.
           */
          result: '=',
          /*
           * List of endpoints that should be shown in
           * the combo
           */
          endpoints: '=',
          /*
           * The default selected endpoint, or the one
           * used in absence of the 'endpoints' list
           */
          defaultEndpoint: '@',
          /**
           * Whether to show or hide the execute
           * button
           */
          hideExecute: '='
        },
        link: function (scope, el, attrs) {


          Prefixes.loadPrefixes();
          var textarea = $(el).find("textarea");


          /*
           * Lov.uriSuggestions()
           * Lov.prefixSuggestions()
           * Sparql.syntaxWords(); // static content //done
           * Prefixes.all()
           */

          scope.checkSuggestion = function (term, callback) {
            var syntax = ['http://www.w3.org/2000/01/rdf-schema#Class',
              'http://www.w3.org/ns/sparql-service-description#Service',
              'http://www.w3.org/2004/02/skos/core#Concept',
              'http://xmlns.com/foaf/0.1/Person',
              'http://schema.org/Person',
              'http://wikidata.dbpedia.org/resource/Q215627',
              'http://www.w3.org/2002/07/owl#Thing',
              'http://wikidata.dbpedia.org/resource/Q5'];

            callback($.map(syntax,//Lov.uriSuggestions(),
              function (s) {
                if (s.toLowerCase().indexOf(term.toLowerCase()) >= 0) {
                  return s;
                } else {
                  return null;
                }
              }));
          };

          scope.checkPrefixSuggestion = function (term, callback) {

            var syntax = [];
            var flag = false;
            var tmp;
            var prefix;
            var suggestion;
            var url;
            if (term.indexOf(":") > 0) {
              flag = true;
              syntax = Prefixes.prefixSuggestions();
              tmp = term.split(":");
              prefix = tmp[0];
              suggestion = tmp[1];
              url = Prefixes.getUrls()[prefix];
            } else {
              syntax = Prefixes.all();
            }

            callback($.map(syntax,  //Lov.prefixSuggestions(),
              function (s) {

                if (!flag && s.toLowerCase().indexOf(term.toLowerCase()) >= 0) {
                  return 0 + s;
                } else if (s.indexOf(url) == 0 && s.toLowerCase().indexOf(suggestion.toLowerCase()) >= 0) {
                  var result = 1 + prefix + "*" + s.substring(url.length) + "*" + s;
                  return result;
                }
                else {
                  return null;
                }

                //var url = Prefixes.getUrls()[prefix];
                /*var url = term;
                 if (s.indexOf(url) == 0 && s.toLowerCase().indexOf(term.toLowerCase()) >= 0) {

                 return s;
                 } else {
                 return null;
                 }*/
              }));
          };

          scope.checkSparqlWord = function (term, callback) {
            callback($
              .map(
              Sparql.syntaxWords(),
              function (word) {
                if (term) {
                  return word
                    .indexOf(term
                      .toUpperCase()) === 0 ? word
                    : null;
                } else {
                  return null;
                }
              }));
          };

          scope.checkPrefix = function (term, callback) {
            callback($
              .map(
              Prefixes.all(),
              function (word) {
                if (term) {
                  var tmp_word = word.toLowerCase();
                  return tmp_word
                    .indexOf(term
                      .toLowerCase()) === 0 ? word
                    : null;
                } else {
                  return null;
                }
              }));
          };

          textarea.textcomplete([{ // <uri
            match: /\B<([\-+\w\.]*)$/,
            search: scope.checkSuggestion,
            replace: function (value) {
              return '<' + value + '>';
            },
            index: 1,
            maxCount: 30
          }, /*{ // prefix
           match: /\b\w+:?\w*$/,
           search: scope.checkPrefix,
           replace: function (value) {
           if (value[0] === value[0].toLowerCase())
           return value + ":";
           else
           return value + " ";
           },
           index: 0
           }, */{
            match: /\b(\w+:?\w*)$/,
            // /\B\{\{([\-+\w\.]*)$/, // {prefix}:{word}
            search: scope.checkPrefixSuggestion,
            template: function (value) {
              if (value[0] == 0) {
                return value.substring(1);
              } else {
                var result = value.split("*");
                return result[1];
              }
            },
            replace: function (value) {
              //textarea.val("PREFIX "+tmp[0]+"\n"+textarea.val());
              //console.log("PREFIX "+tmp[0]+"\n"+textarea.text());
              if (value[0] == 0) {
                if (value[1] == value[1].toLowerCase()) {
                  return value.substring(1) + ":";
                } else {
                  return value.substring(1) + " ";
                }
              } else {
                var result = value.split("*");
                return result[0].substring(1) +":"+result[1]+" ";
              }
            },
            index: 1,
            maxCount: 30
          }]);
        },
        controller: function ($scope) {
          $scope.endpoint = $scope.defaultEndpoint;
          $scope.executeQuery = function () {
            //execute the query and save the result
            $scope.result = null; //returned result from the endpoint
          }

        }
      };
    }]);







