/**
 * Created by Aleksandar on 18/5/2015.
 */

FirstApp.directive('semEntity', [
  '$compile',
  '$templateCache',
  '$http',
  'SemService',
  '$filter',
  'crudService',
  'ngTableParams',
  'SemEntityService',
  function ($compile, $templateCache, $http, SemService, $filter, crudService, SemEntityService) {
    return {
      restrict: 'EA',
      scope: {
        rdfType: '@',
        semResource: '=',
        uri: '@',
        displayMode: '@'
      },
      template: '<div></div>',
      controller: function ($scope, crudService, ngTableParams, SemEntityService, SemService) {
        $scope.types = [];
        $scope.modes = ["List", "Preview", "Edit", "CustomPreview"];
        $scope.supportedTypes = ['default'];
        $scope.currentSupportedTypes = ['default'];
        $scope.customAttribute = "";
        $scope.semanticAttribute = {};

        var semAttService = crudService('semantic_attributes');


        $scope.urls = {
          "Albert Einstein": "http://dbpedia.org/data/Albert_Einstein.json",
          "Alan Turing": "http://dbpedia.org/data/Alan_Turing.json",
          "Elvis Presley": "http://dbpedia.org/data/Elvis_Presley.json",
          "Harry Potter": "http://dbpedia.org/data/Harry_Potter.json",
          "New York": "http://dbpedia.org/data/New_York_City.json"
        };

        $scope.runQuery = function () {
          SemService.runSparqlQuery($scope.sparql_query, function (data) {
            console.log(data);
          });
        }

        $scope.editAttr = function (entity) {
          angular.forEach($scope.semResource, function (value, key) {
            if (key == entity.type) {
              $scope.semanticAttr = value;
            }
          })
          $scope.customAttribute = entity;
        }

        $scope.editEntity = function (entity) {
          angular.forEach($scope.types, function (value) {
            if (value == entity.type) {
              $scope.comboType = value;
            }
          })
          $scope.customEntity = entity;
        }

        $scope.semanticEntities = SemEntityService.query({}, function (data) {
          angular.forEach(data, function (value) {
            $scope.supportedTypes.push(value.type);
          });
          $scope.tableParams3 = new ngTableParams({
            page: 1,            // show first page
            count: 5           // count per page
          }, {
            total: data.length, // length of data
            getData: function ($defer, params) {
              var orderedData = params.sorting() ? $filter('orderBy')($scope.semanticEntities, params.orderBy()) : $scope.semanticEntities;
              $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });
        });


        $scope.availableCustomAttrs = semAttService.query({}, function (data) {
          $scope.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: 5           // count per page
          }, {
            total: data.length, // length of data
            getData: function ($defer, params) {
              var orderedData = params.sorting() ? $filter('orderBy')($scope.availableCustomAttrs, params.orderBy()) : $scope.availableCustomAttrs;
              $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });

          $scope.tableParams2 = new ngTableParams({
            page: 1,            // show first page
            count: 5           // count per page
          }, {
            total: data.length, // length of data
            getData: function ($defer, params) {
              var orderedData = params.sorting() ? $filter('orderBy')($scope.availableCustomAttrs, params.orderBy()) : $scope.availableCustomAttrs;
              $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });
        });

        $scope.saveCustomAttribute = function () {
          semAttService.save($scope.customAttribute, function () {
            $scope.customAttribute = {};
            $scope.availableCustomAttrs = semAttService.query();
          });
        };

        $scope.saveCustomEntity = function () {
          $scope.customEntity.type = $scope.comboType;
          SemEntityService.save($scope.customEntity, function () {
            $scope.customEntity = {};
            $scope.semanticEntities = SemEntityService.query({}, function (data) {
              $scope.supportedTypes = data;
            });
          });
        };

        $scope.saveEditedEntity = function () {
          SemEntityService.save($scope.editedEntity, function (data) {
            $scope.semanticEntities = SemEntityService.query({}, function (data) {
              $scope.setPreviewEntity();
            });

          });
        };

        $scope.updateURI = function (uri) {
          $scope.uri = uri;
        }

        $scope.updateType = function (type) {
          if (type) {
            $scope.rdfType = type;
          } else {
            $scope.rdfType = $scope.selectedProperty;
          }
        }


        $scope.reload = function () {
          /**
           * sending: http://dbpedia.org/page/Albert_Einstein
           * receiving: json representaton of the resource
           */
          if (typeof $scope.uri === 'undefined') {
            return;
          }
          if ($scope.uri.indexOf("/resource/") != -1) {
            $scope.uri = $scope.uri.replace("/resource/", "/data/");
          }
          if ($scope.uri.indexOf(".json") != $scope.uri.length - 5) {
            $scope.uri += ".json";
          }
          SemService.load($scope.uri, function (semResource) {
            var res = $scope.uri.substring(0, $scope.uri.length - 5).replace("data", "resource");
            $scope.semResource = semResource[res];
            console.log($scope.semResource);
          });
        }

        $scope.changeDisplayMode = function (mode) {
          $scope.displayMode = mode.toLowerCase();
          if ($scope.displayMode == "list") {
            $scope.rdfType = "default";
          }
        }
      },
      link: function (scope, element, attrs) {


        scope.previewCustomEntity = function () {
          var el = element.find("#customEntityPreview");
          if (scope.customEntity) {
            el.html('');
            el.append($compile(scope.customEntity.html)(scope));
          }
        };

        scope.previewCustomAttribute = function () {
          var el = element.find("#customAttributePreview");
          if (scope.customAttribute) {
            el.html('');
            el.append($compile(scope.customAttribute.html)(scope));
          }
        };

        scope.setPreviewEntity = function () {
          console.log(scope.previewEntity.html);

          var preview_element = element.find('#previewEntity');
          preview_element.html('');
          preview_element.append($compile(scope.previewEntity.html)(scope));
        }

        scope.previewEditedEntity = function () {
          var el = element.find("#editedEntity");
          console.log(el);
          el.html("");
          el.append($compile(scope.editedEntity.html)(scope));
        };

        scope.resetCustomAttribute = function () {
          var el = element.find("#customAttributePreview");
          el.html('');
          scope.semanticAttr = {};
          scope.customAttribute = {};
        };

        scope.resetCustomEntity = function () {
          var el = element.find("#customEntityPreview");
          el.html('');
          scope.customEntity = {};
          scope.comboType = {};
        };

        scope.$watch('uri', function (value) {
          scope.reload();
        });


        scope.$watch('rdfType', function (value) {
          loadTemplate(scope.rdfType, scope.displayMode);
        });

        scope.$watch('semResource', function (value) {
          if (value) {
            scope.types = ["default"];
            scope.currentSupportedTypes = ["default"];
            angular.forEach(value['http://www.w3.org/1999/02/22-rdf-syntax-ns#type'], function (type) {
              scope.types.push(type.value);
              if (is.inArray(type.value, scope.supportedTypes)) {
                scope.currentSupportedTypes.push(type.value);
              }
            });
            var default_type = true;
            angular.forEach(scope.semanticEntities, function (val) {
              if (is.inArray(val, scope.types)) {
                default_type = false;
                scope.selectedProperty = val;
                scope.displayMode = 'preview';
                scope.type = ($filter('urlExtractor')(val)).toLowerCase();
              }
            });
            if (default_type == true) {
              scope.type = "default";
              scope.selectedProperty = "default";
            }
            loadTemplate(scope.type, scope.displayMode);
          }
        });


        scope.$watch('displayMode', function (value) {
          loadTemplate(scope.rdfType, scope.displayMode);
        });

        function loadTemplate(template, type) {
          var path = 'entities/' + type + '.html';
          $http.get(path, {cache: $templateCache})
            .success(function (templateContent) {
              element.html('');
              element.append($compile(templateContent)(scope));
              if (type != "list") {
                scope.previewEntity = $filter('filter')(scope.semanticEntities, {type: template})[0];
                scope.editedEntity = {};
                angular.copy(scope.previewEntity, scope.editedEntity);
                scope.setPreviewEntity();
              }
            });
        }

        var name = scope.rdfType || (scope.semResource && scope.semResource.type);
        if (name) {
          loadTemplate(name, scope.displayMode);
        }
      }
    }
      ;
  }])
;
