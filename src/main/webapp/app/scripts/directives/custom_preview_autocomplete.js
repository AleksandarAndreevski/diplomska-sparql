/**
 * Created by Aleksandar on 18/5/2015.
 */

FirstApp.directive(
  'customPreviewAutocomplete',
  [
    function() {
      return {
        restrict: 'EA',
        // templateUrl: 'templates/textarea.html',
        scope: {
          /**
           * the query that will be generated with
           * autocomplete
           */
          semResourceObject: '=',
          /**
           * The result of the query execution. Null
           * if execute is not invoked.
           */
          result: '=',
          /*
           * List of endpoints that should be shown in
           * the combo
           */
          endpoints: '=',
          /*
           * The default selected endpoint, or the one
           * used in absence of the 'endpoints' list
           */
          defaultEndpoint: '@',
          /**
           * Whether to show or hide the execute
           * button
           */
          hideExecute: '='
        },
        link: function(scope, el, attrs) {
          var sem_properties = [];
          var sem_values = [];
          var sem_links = [];
          angular.forEach(scope.semResourceObject, function(value, key) {
            sem_properties.push(key);
            sem_links.push(key);
            angular.forEach(value, function(val) {
              if(val.type == 'uri'){
                sem_values.push(val.value);
                sem_links.push(val.value);
              }
            });
          });
          var html_tags = ['span', 'div', 'h1', 'h2', 'h3', 'img', 'p', 'label', 'textarea', 'input', 'button', 'sem-attribute'];
          var textarea = $(el).find("textarea");
          textarea.textcomplete([{
            match: /url=(\w*)/,
            search: function(term, callback) {
              callback($.map(sem_links, function(element) {
                return element.indexOf(term) >= 0 ? element : null;
              }));
            },
            replace: function(value) {
              return 'attr-type="\''+value+'\'"';
            },
            index: 1
          }, { // <html
            match: /<(\w*)$/,
            search: function(term, callback) {
              callback($.map(html_tags, function(element) {
                return element.indexOf(term) === 0 ? element : null;
              }));
            },
            index: 1,
            replace: function(element) {
              return ['<' + element + '>', '</' + element + '>'];
            }
          }]);
        },
        controller: function($scope) {

        }
      };
    }
  ]);







