/**
 * Created by Aleksandar on 31/7/2015.
 */
FirstApp.directive('semAttribute', [
  '$compile',
  '$templateCache',
  'SemAttrService',
  function ($compile, $templateCache, $http, SemAttrService) {
    return {
      restrict: 'E',
      scope: {
        attrType: '=',
        entity: '@',
        navigateClickFunc: "="
      },
      template: '<div></div>',
      controller: function ($scope, SemAttrService) {

        $scope.entity = SemAttrService.getByType({
          type: $scope.attrType
        }, function (data) {
          $scope.entity = data;
        });


        $scope.semResource = $scope.$parent.semResource;


        $scope.navigateFunction = function (value) {
          if ($scope.navigateClickFunc && typeof $scope.navigateClickFunc === 'function') {
            $scope.navigateClickFunc(value);
          }
        }


      },
      link: function (scope, element, attrs) {

        scope.$watch('entity', function (value) {
          if (value && value.html) {
            element.html('');
            var tmp_html = value.html.replace(/semanticAttr/g, 'semResource["' + value.type + '"]');
            element.append($compile(tmp_html)(scope));
          }
        });

      }
    };
  }])
;
