/**
 * Created by Aleksandar on 29/7/2015.
 */
FirstApp.filter('urlExtractor', function() {
  return function(input) {
    var str = input.substring(input.lastIndexOf("/") + 1);
    if(str.indexOf("#") != -1) {
      return str.substring(str.lastIndexOf("#") + 1);
    }
    return str;
  };
});
