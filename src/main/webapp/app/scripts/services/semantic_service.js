/**
 * Created by Aleksandar on 18/5/2015.
 */
FirstApp.factory('SemService', ['$http', function ($http) {

  var sparqlQuery = {};

  sparqlQuery.getQueryResult = function (param) {
    $http.get('/query/param/' + param).success(function (data) {
      return data;
    });
  }

  sparqlQuery.load = function (uri, callback) {
    $http.post('/query', $.param({url: uri}), {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).success(callback);
  }

  sparqlQuery.runSparqlQuery = function (query, callback) {
    $http.post('/sparql', $.param({query: query}), {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).success(callback);
  }

  return sparqlQuery;

}]);
