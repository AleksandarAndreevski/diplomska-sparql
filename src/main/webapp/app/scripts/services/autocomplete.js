/**
 * Created by Aleksandar on 18/5/2015.
 */

FirstApp.factory('Sparql', [function () {
  var sparql = {};

  sparql.syntaxWords = function () {
    var words = ["WHERE", "ASK", "FROM", "SELECT", "PREFIX", "UPDATE", "CONSTRUCT", "FILTER"];
    return words;
  }

  return sparql;
}]);

FirstApp.factory('Prefixes', ['$http', function ($http) {
  var prefixes = {};

  var prefix_words = ["BASE", "ASK", "DISTINCT", "NAMED", "UNION", "ORDER", "BY",
    "STR", "DATATYPE", "ISIRI", "ISLITERAL", "PREFIX", "CONSTRUCT", "REDUCED", "WHERE", "FILTER",
    "LIMIT", "ASC", "LANG", "BOUND", "ISURI", "REGEX", "SELECT", "DESCRIBE", "FROM", "GRAPH", "OPTIONAL",
    "OFFSET", "DESC", "LANGMATCHES", "SAMETERM", "ISBLANK"];

  var prefix_urls = {};


  prefixes.loadPrefixes = function () {
    $http.get('http://prefix.cc/popular/all.file.json').
      success(function (data, status, headers, config) {
        $.each(data, function (k, v) {
          prefix_words.push(k);
        });
        prefix_urls = data;
      }).
      error(function (data, status, headers, config) {
        console.log(data);
        console.log(status);
      });
  }


  prefixes.all = function () {
    return prefix_words;
  }

  prefixes.getUrls = function () {
    return prefix_urls;
  }

  prefixes.prefixSuggestions = function() {
    return ['http://www.w3.org/2000/01/rdf-schema#Class',
      'http://www.w3.org/ns/sparql-service-description#Service',
      'http://www.w3.org/2004/02/skos/core#Concept',
      'http://xmlns.com/foaf/0.1/Person',
      'http://schema.org/Person',
      'http://wikidata.dbpedia.org/resource/Q215627',
      'http://www.w3.org/2002/07/owl#Thing',
      'http://wikidata.dbpedia.org/resource/Q5',
      'http://xmlns.com/foaf/0.1/primaryTopic',
      'http://xmlns.com/foaf/0.1/homepage',
      'http://xmlns.com/foaf/0.1/isPrimaryTopicOf',
      'http://xmlns.com/foaf/0.1/name',
      'http://xmlns.com/foaf/0.1/Organization'];
  }

  return prefixes;

}]);
