/*
 * Generic CRUD resource REST service
 */
FirstApp.factory('SemAttrService', ['$resource', function($resource) {

  return $resource('/data/rest/semantic_attributes/:id', {}, {
    getByType: {
      method: 'GET',
      url: "/data/rest/semantic_attributes/get_by_type",
      params:{
        type: '@type'
      }
    }
  });

}]);
