/*
 * Generic CRUD resource REST service
 */
FirstApp.factory('SemEntityService', ['$resource', function($resource) {

  return $resource('/data/rest/semantic_entities/:id', {}, {
    getByType: {
      method: 'GET',
      url: "/data/rest/semantic_entities/get_by_type",
      params:{
        type: '@type'
      }
    }
  });

}]);
