package mk.ukim.finki.wp.service.impl;

import mk.ukim.finki.wp.model.SemanticAttribute;
import mk.ukim.finki.wp.repository.SemanticAttributeRepository;
import mk.ukim.finki.wp.service.SemanticAttributeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Aleksandar on 20/8/2015.
 */
@Service
public class SemanticAttributeServiceImpl extends BaseEntityCrudServiceImpl<SemanticAttribute, SemanticAttributeRepository> implements SemanticAttributeService {

    @Autowired
    SemanticAttributeRepository repository;

    @Override
    protected SemanticAttributeRepository getRepository() {
        return repository;
    }


    @Override
    public SemanticAttribute getByType(String type) {
        return repository.findByType(type);
    }
}
