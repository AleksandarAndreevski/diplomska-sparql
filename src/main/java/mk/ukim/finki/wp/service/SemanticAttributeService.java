package mk.ukim.finki.wp.service;

import mk.ukim.finki.wp.model.SemanticAttribute;

import java.util.List;

/**
 * Created by Aleksandar on 20/8/2015.
 */
public interface SemanticAttributeService extends BaseEntityCrudService<SemanticAttribute> {
    SemanticAttribute getByType(String type);
}
