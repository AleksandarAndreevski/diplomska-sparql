package mk.ukim.finki.wp.service;

import mk.ukim.finki.wp.model.SemanticAttribute;
import mk.ukim.finki.wp.model.SemanticEntity;

/**
 * Created by Aleksandar on 20/8/2015.
 */
public interface SemanticEntityService extends BaseEntityCrudService<SemanticEntity> {
    SemanticEntity getByType(String type);
}
