package mk.ukim.finki.wp.service.impl;

import mk.ukim.finki.wp.model.SemanticAttribute;
import mk.ukim.finki.wp.model.SemanticEntity;
import mk.ukim.finki.wp.repository.SemanticAttributeRepository;
import mk.ukim.finki.wp.repository.SemanticEntityRepository;
import mk.ukim.finki.wp.service.SemanticAttributeService;
import mk.ukim.finki.wp.service.SemanticEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Aleksandar on 20/8/2015.
 */
@Service
public class SemanticEntityServiceImpl extends BaseEntityCrudServiceImpl<SemanticEntity, SemanticEntityRepository> implements SemanticEntityService {

    @Autowired
    SemanticEntityRepository repository;

    @Override
    protected SemanticEntityRepository getRepository() {
        return repository;
    }


    @Override
    public SemanticEntity getByType(String type) {
        return repository.findByType(type);
    }
}
