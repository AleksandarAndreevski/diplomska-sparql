package mk.ukim.finki.wp.repository;

import mk.ukim.finki.wp.model.SemanticAttribute;
import mk.ukim.finki.wp.model.SemanticEntity;

/**
 * Created by Aleksandar on 20/8/2015.
 */
public interface SemanticEntityRepository extends JpaSpecificationRepository<SemanticEntity> {
    SemanticEntity findByType(String type);
}
