package mk.ukim.finki.wp.repository;

import mk.ukim.finki.wp.model.SemanticAttribute;

/**
 * Created by Aleksandar on 20/8/2015.
 */
public interface SemanticAttributeRepository extends JpaSpecificationRepository<SemanticAttribute> {
    SemanticAttribute findByType(String type);
}
