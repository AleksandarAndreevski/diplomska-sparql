package mk.ukim.finki.wp.web;

import mk.ukim.finki.wp.service.SemanticAttributeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Aleksandar on 18/5/2015.
 */

@Controller
public class EndpointController {



    @RequestMapping(value = "/sparql", method = RequestMethod.POST)
    public void runSparqlQuery(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String query = request.getParameter("query");
        String encoded_query = URLEncoder.encode("default-graph-uri=http://dbpedia.org&query="+query+"&format=json&timeout=0","UTF-8");
        String url = "http://dbpedia.org/sparql?"+encoded_query;
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(url, String.class);
        PrintWriter pw = response.getWriter();
        pw.printf("%s", result);
        pw.flush();
        pw.close();
    }

    @RequestMapping(value = "/query", method = RequestMethod.POST)
    public void getQueryResultsByParam(/*@RequestParam(value = "url", required = false) String url,*/ HttpServletRequest request, HttpServletResponse response) throws IOException {
        String url = request.getParameter("url");
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(url, String.class);
        PrintWriter pw = response.getWriter();
        pw.printf("%s", result);
        pw.flush();
        pw.close();
    }


    @RequestMapping(value = "/query/param/{param}", method = RequestMethod.GET)
    public void getQueryResultsByUrl(@PathVariable String param, HttpServletRequest request, HttpServletResponse response) throws IOException {
        Map<String, String> vars = new HashMap<String, String>();
        vars.put("query_param", param);
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject("http://dbpedia.org/data/{query_param}.json", String.class, vars);
        PrintWriter pw = response.getWriter();
        pw.printf("%s", result);
        pw.flush();
        pw.close();
    }



}
