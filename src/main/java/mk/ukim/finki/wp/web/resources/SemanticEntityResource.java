package mk.ukim.finki.wp.web.resources;

import mk.ukim.finki.wp.model.SemanticAttribute;
import mk.ukim.finki.wp.model.SemanticEntity;
import mk.ukim.finki.wp.service.SemanticAttributeService;
import mk.ukim.finki.wp.service.SemanticEntityService;
import mk.ukim.finki.wp.web.CrudResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/data/rest/semantic_entities")
public class SemanticEntityResource extends
        CrudResource<SemanticEntity, SemanticEntityService> {

    @Autowired
    private SemanticEntityService service;

    @Override
    public SemanticEntityService getService() {
        return service;
    }

    @RequestMapping(value = "/get_by_type", method = RequestMethod.GET, produces = "application/json")
    public SemanticEntity getByType(@RequestParam String type) {
        return service.getByType(type);
    }

}
