package mk.ukim.finki.wp.web.resources;

import mk.ukim.finki.wp.model.SemanticAttribute;
import mk.ukim.finki.wp.service.SemanticAttributeService;
import mk.ukim.finki.wp.web.CrudResource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/data/rest/semantic_attributes")
public class SemanticAttributeResource extends
        CrudResource<SemanticAttribute, SemanticAttributeService> {

    @Autowired
    private SemanticAttributeService service;

    @Override
    public SemanticAttributeService getService() {
        return service;
    }

    @RequestMapping(value = "/get_by_type", method = RequestMethod.GET, produces = "application/json")
    public SemanticAttribute getByType(@RequestParam String type) {
        return service.getByType(type);
    }

}
