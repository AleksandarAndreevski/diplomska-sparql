package mk.ukim.finki.wp.web;

import mk.ukim.finki.wp.service.SemanticAttributeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Aleksandar on 18/5/2015.
 */

@RestController
public class SemanticEntityController {


    @RequestMapping(value = "/save_file/{filename}", method = RequestMethod.POST)
    public void saveFile(@PathVariable String filename, @RequestBody String html, HttpServletRequest request, HttpServletResponse response) throws IOException {
        File file = new File("./src/main/webapp/entities" + filename + ".html");

        if(!file.exists()){
            file.mkdirs();
        }

        FileOutputStream fileOutputStream = new FileOutputStream(file);
        PrintWriter printWriter = new PrintWriter(fileOutputStream);

        printWriter.write(html);

        printWriter.close();
        fileOutputStream.close();
    }


    @RequestMapping(value = "/get_all_files", method = RequestMethod.GET)
    public String[] getFilenames(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String[] filenames = new String[0];
        File file = new File("./src/main/webapp/entities");
        if(file.exists()){
            filenames = file.list();
        }
        return filenames;
    }

}
